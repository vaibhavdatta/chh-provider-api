package com.chh.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 * 
 * 
 * The @EnableSwagger2 annotation enables Swagger 2 support by registering certain
 * beans into the Spring application context.
 * 
 * The @Import annotation imports additional classes into the Spring application
 * context that are needed to automatically create a Swagger documentation from
 * our Spring Data REST repositories.
 */

@SpringBootApplication
@EnableSwagger2WebMvc
@Import(SpringDataRestConfiguration.class)
public class ChhProviderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChhProviderApiApplication.class, args);
	}

}
